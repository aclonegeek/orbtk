use crate::prelude::*;

property!(
    /// `MouseOver` describes the mouse over state of a widget.
    MouseOver(bool)
);
