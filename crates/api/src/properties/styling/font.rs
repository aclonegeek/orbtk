use crate::prelude::*;

property!(
    /// `Font` describes the text font of a widget.
    Font(String) : &str
);
