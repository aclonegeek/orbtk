use crate::{prelude::*, utils::*};

property!(
    /// `Background` describes the background brush of a visual element.
    Background(Brush) : &str,
    String
);
